'''
Based on: move_group_python_interface_tutorial.py
Written by: Acorn Pooley, Mike Lautman
Credits: Aaric Han, Juan Lasso Velasco
'''
#%% Imports
import sys
import copy
import rospy
import moveit_commander as mic
import numpy as np

#%% Initial setup
mic.roscpp_initialize(sys.argv) #starts moveit_commander
rospy.init_node("move_group_python_interface_tutorial", anonymous=True) #rospy node
robot = mic.RobotCommander() #robot object to control states
group_name = "manipulator" #picks the robot's joints
move_group = mic.MoveGroupCommander(group_name) #object to actually move the robot

#%% Avoid Singularities
def ready(goal=[0, -1.5447, 1.5447, -1.5447, -1.5447, 0]): #optional change of start config
    joint_goal = move_group.get_current_joint_values() #gets the current pose
    for i in range(len(goal)): #changes the pose to the goal values given in function.
        joint_goal[i] = goal[i]
    move_group.go(joint_goal, wait=True) #executes the plan
    move_group.stop() #forces residual movement to stop.

#%% Change positions
letters = {"g": np.array([[0.2, 0, 0, 0.2, 0.2, 0.1],[0.4, 0.4, 0, 0, 0.2, 0.2]]),
           "t": np.array([[0.1,0.1,0,0.2],[0,0.4,0.4,0.4]]),
           "m": np.array([[0, 0,0.1,0.2,0.2],[0,0.4,0.2,0.4,0]])} #would need to change for other letters.
letters["g"] = letters["g"] + 0.1 #adds a factor of safety or robot would start at 0.
letters["t"] = letters["t"] + 0.1
letters["m"] = letters["m"] + 0.1
def set_go(fi="g", si="t", ti="m"):
    given = [fi, si, ti]
    for l in given: #loop to do each given letter.
        waypoints = [] #empty list of points.
        wpose = move_group.get_current_pose().pose #where the robot is now.
        wpose.position.z=0.25 #want constant height while drawing.
        for i in range(len(letters[l][0])): #will iterate depending on numbers of points in each letter.
            wpose.position.y = letters[l][1][i] #changes to the y-value given by dict.
            wpose.position.x = letters[l][0][i] #changes to the x-value given by dict.
            waypoints.append(copy.deepcopy(wpose)) #adds to list of points
        (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0) #plans the path.
        move_group.execute(plan, wait=True) #actually moves the robot.
        move_group.stop() #stops all residual movement
        ready() #goes back to home base.
#%% Move to home pose.
print("")
use_goal = input("I'm feeling homesick. Is it okay if I go to base? y or n")

if use_goal=='y':
    print("")
    print("Thank you!")
    ready()
elif use_goal=='n': #changes the starting position.
    new_goal=input('Where should I go instead? Please enter a joint goal in radians. e.g [1,2,3,4,5,6].')
    print("")
    print("That sounds like a great spot! Moving there now.")
    ready(goal=new_goal)
else:
    print("")
    print("Sorry I didn't understand that :( Running with default settings.")
    ready()

#%% Draw initials.
print("")
input("Type enter to begin.") #if I did more letters, I would ask for them here.
print("Drawing now. Please go check out my artwork.")
set_go() #if input was added this becomes =set_go(new_letters)

